#!/bin/bash

set -euo pipefail

# directory of this script
HERE=$(cd `dirname $0` && pwd)

# Top directory of the NSD config area. Contains config and
# runtime dirs that need to be bind-mounted into the container.
BASE=$(cd "$HERE"/ && pwd)

podman run -d \
    -v $BASE/var-lib-nsd:/var/lib/nsd:z \
    -v $BASE/etc-nsd:/etc/nsd:z \
    --ip 172.16.128.53 \
    --name pod-nsd \
    -p 53   -p 53/udp \
    nsd \
    nsd

podman generate systemd pod-nsd > $BASE/pod-nsd.service

echo New service file: $BASE/pod-nsd.service
read -p DO_COPY "Copy to /etc/systemd/system? [y/N]: "
[ "x$DO_COPY" == "xy" ] && cp $BASE/pod-nsd.service /etc/systemd/system/

