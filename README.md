# nsd-image

Container image for nsd. Expects the following mounts:

  - ```/var/lib/nsd``` for runtime data
  - ```/etc/nsd``` for config data, config files etc.

Recommended directory layout is as follows:

  - ```$BASE```
    - ```etc-nsd/```
      - ```nsd.conf```
      - ```zonefiles/...```
    - ```var-lib-nsd/```
      - ```db/...```
    - ```nsd-image/tools/...``` (i.e. this repo)

For conveninence, or documentational reference: the container be run
using the ```./tools/run.sh``` script. The script will also create
